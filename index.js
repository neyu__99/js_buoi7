var Arr=[]
var Ex9Arr=[]

document.getElementById('ThemN').onclick=function (){
    const n=Number(document.getElementById('SoN').value)
    Arr.push(n)
    document.querySelector('#PrArray').innerHTML=Arr
    // sắp xếp tăng dần
    var ArrSort=(Arr.slice()).sort(function(a,b){return a-b})
        
    // ex1
    document.getElementById('Ex1Btn').onclick=function (){
        var Ex1Result=0
        for (let index=0; index<Arr.length;index++){
            const num=Arr[index]
            if (num>=0){
                Ex1Result+=num
                document.getElementById('Ex1Result').innerHTML=`Tổng số dương: ${Ex1Result}`
            }
        }
    }
    // ex2
    document.getElementById('Ex2Btn').onclick=function (){
        var Ex2Result=0
        for (let index=0; index<Arr.length;index++){
            const num=Arr[index]
            if (num>0){
                Ex2Result++
                document.getElementById('Ex2Result').innerHTML=`Số dương: ${Ex2Result}`
            }
        }
    }
    // ex3
    document.getElementById('Ex3Btn').onclick=function (){
        document.getElementById('Ex3Result').innerHTML=`Số nhỏ nhất: ${ArrSort[0]}`
    }
    // ex4
    document.getElementById('Ex4Btn').onclick=function (){
        for (let index=0; index<Arr.length;index++){
            const num=ArrSort[index]
            if (num>0){
                document.getElementById('Ex4Result').innerHTML=`Số dương nhỏ nhất: ${num}`
                break
            }
        }
    }
    // ex5
    document.getElementById('Ex5Btn').onclick=function (){
        var Ex5Result=0
        for (let index=0; index<Arr.length;index++){
            const num=Arr[index]
            if (num%2==0){
                Ex5Result=num
                document.getElementById('Ex5Result').innerHTML=`Số chẵn cuối cùng: ${Ex5Result}`
            }
        }
    }
    // ex6
    document.getElementById('Ex6Btn').onclick=function (){
        var Ex5Result=0
        var Inx1=document.getElementById('Ex6Num1').value
        var Inx2=document.getElementById('Ex6Num2').value
        var Bien=Arr[Inx1]
        Arr[Inx1]=Arr[Inx2]
        Arr[Inx2]=Bien
        document.getElementById('Ex6Result').innerHTML=`Mảng sau khi đổi: ${Arr}`
    }
    // ex7
    document.getElementById('Ex7Btn').onclick=function (){
        document.getElementById('Ex7Result').innerHTML=`Mảng sau khi sắp xếp: ${ArrSort}`
    }
    // ex8
    document.getElementById('Ex8Btn').onclick=function (){
        var Ex8Result
        outerloop: for (let index=0; index<Arr.length;index++){
            const num=Arr[index]
            console.log("🚀 ~ file: index.js:76 ~ num", num)
            
            if ((num==2)||(num==3)){
                Ex8Result=num
                break
            }
            else if (num>4){
                for (var i=2;i<=Math.sqrt(num);i++){
                    if (num%i==0){
                        break outerloop
                    }
                    else {
                        if (i<Math.floor(Math.sqrt(num))){
                            void(0)
                        }
                        else {
                            Ex8Result=num
                            break outerloop
                        }
                    }
                }
            }
            else {Ex8Result='không có số nguyên tố'}
        }
        document.getElementById('Ex8Result').innerHTML=`Số nguyên tố đầu tiên: ${Ex8Result}`
    }
    // ex10
    document.getElementById('Ex10Btn').onclick=function (){
        var Soam=0
        var Soduong=0
        for (let index=0; index<Arr.length;index++){
            const num=Arr[index]
            if (num>0){
                Soduong++
            }
            else if (num<0){
                Soam++
            }
            console.log(Soduong,Soam);
        }
        if (Soduong>Soam){document.getElementById('Ex10Result').innerHTML=`Số dương > Số âm`}
        else if (Soduong<Soam){document.getElementById('Ex10Result').innerHTML=`Số dương < Số âm`}
        else if (Soduong=Soam){document.getElementById('Ex10Result').innerHTML=`Số dương = Số âm`}
    }   
}

// ex9
document.getElementById('Ex9BtnThem').onclick=function (){
    const Ex9n=Number(document.getElementById('Ex9Input').value)
    console.log("🚀 ~ file: index.js:112 ~ Ex9n", Ex9n)
    Ex9Arr.push(Ex9n)
    document.querySelector('#Ex9Them').innerHTML=Ex9Arr
    document.getElementById('Ex9Btn').onclick=function (){
        var Ex9Result=0
        for (let index=0; index<Ex9Arr.length;index++){
            const Ex9num=Ex9Arr[index]
            if (Number.isInteger(Ex9num)==true){
                Ex9Result++
                document.getElementById('Ex9Result').innerHTML=`Số nguyên: ${Ex9Result}`
            }
        }
    }
}


